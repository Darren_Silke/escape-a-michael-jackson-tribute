﻿using UnityEngine;

// Enemy generic behaviour.
public class EnemyScript : MonoBehaviour
{
	// Component references.
	private MoveScript moveScript;
	private AIScript aiScript;
	private WeaponScript[] weapons;

	// Game data object.
	private GameObject gameData;

	// Internal variables.
	private bool hasSpawn;
	private float spawnDelay;

	// Method called upon initialization.
	// Method retrieves references between scripts.
	void Awake ()
	{
		// Retrieve the weapon only once.
		weapons = GetComponentsInChildren<WeaponScript> ();

		// Retrieve scripts to disable when not spawn.
		if (gameObject.name.Contains ("AI")) {
			aiScript = GetComponent<AIScript> ();
		} else {
			moveScript = GetComponent<MoveScript> ();
		}
	}

	// Method called upon initialization.
	// Disable everything.
	void Start ()
	{
		spawnDelay = Random.Range (0f, 2f);
		hasSpawn = false;

		// Disable everything.
		// -- Collider.
		collider2D.enabled = false;
		// -- Moving.
		if (gameObject.name.Contains ("AI")) {
			aiScript.moveSpeed = 0;
		} else {
			moveScript.direction.y = 0;
		}
	
		// -- Shooting.
		foreach (WeaponScript weapon in weapons) {
			weapon.enabled = false;
		}

		gameData = GameObject.Find ("Game Data");
	}

	// Update is called once per frame.
	void Update ()
	{
		// Check if the enemy has spawned.
		if (hasSpawn == false) {
			if (renderer.IsVisibleFrom (Camera.main)) {
				Invoke ("Spawn", spawnDelay);
			}
		} else {
			// Auto-fire.
			foreach (WeaponScript weapon in weapons) {
				if ((weapon != null) && (weapon.enabled) && (weapon.CanAttack)) {
					weapon.Attack (true, 0);
				}
			}

			// Out of the camera? De-activate the game object.
			if (renderer.IsVisibleFrom (Camera.main) == false) {
				Start ();
				// If the Enemy goes beyond the game world, destroy it.
				// Only really applies to Level 1.
				if (transform.position.y <= -6) {
					gameData.GetComponent<GameDataScript> ().DecreaseEnemyCount ();
					Destroy (gameObject);
				}
			}
		}
	}

	// Spawn method to make the enemy spawn.
	// Activate itself.
	private void Spawn ()
	{
		hasSpawn = true;

		// Enable everything.
		// -- Collider.
		collider2D.enabled = true;
		// -- Moving.
		if (gameObject.name.Contains ("AI")) {
			aiScript.moveSpeed = 1;
			aiScript.rotationSpeed = 10;
		} else {
			moveScript.direction.y = -1;
		}
		// -- Shooting.
		foreach (WeaponScript weapon in weapons) {
			weapon.enabled = true;
		}
	}
}
