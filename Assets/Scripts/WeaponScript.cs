﻿using UnityEngine;

// Launch projectile.
public class WeaponScript : MonoBehaviour
{
	// Projectile prefab for shooting.
	public Transform shotPrefab;
	
	// Cooldown in seconds between two shots.
	public float shootingRate = 0.5f;

	// Cooldown.
	private float shootCooldown;

	// Method called upon initialization.
	void Start ()
	{
		shootCooldown = 0f;
	}

	// Update is called once per frame.
	void Update ()
	{
		if (shootCooldown > 0) {
			shootCooldown -= Time.deltaTime;
		}
	}

	// Shooting from another script.
	// Create a new projectile if possible.
	public void Attack (bool isEnemy, float yOffset)
	{
		if (CanAttack) {
			shootCooldown = shootingRate;

			// Create a new shot.
			var shotTransform = Instantiate (shotPrefab) as Transform;
			// Assign position.
			shotTransform.position = new Vector3 (transform.position.x, transform.position.y + yOffset, transform.position.z);
			// The 'Is Enemy' property.
			ShotScript shot = shotTransform.gameObject.GetComponent<ShotScript> ();
			if (shot != null) {
				shot.isEnemyShot = isEnemy;
			}

			if ((gameObject.name.Equals ("Michael")) || (gameObject.name.Equals ("Bodyguard"))) {
				SoundEffectsHelperScript.Instance.MakePlayerShootSound ();
			} else if ((gameObject.name.Equals ("WeaponObject 1")) && (gameObject.transform.parent.name.Equals ("Boss Enemy"))) {
				SoundEffectsHelperScript.Instance.MakeBossEnemyShootSound ();
			} else if ((gameObject.name.Equals ("WeaponObject 1")) && (gameObject.transform.parent.name.Contains ("AI Enemy"))) {
				SoundEffectsHelperScript.Instance.MakeAIEnemyShootSound ();
			} else if (gameObject.name.Equals ("WeaponObject")) {				
				SoundEffectsHelperScript.Instance.MakeEnemyShootSound ();
			}
		}
	}
	
	// Is the weapon ready to create a new projectile?
	public bool CanAttack {
		get {
			return shootCooldown <= 0f;
		}
	}
}
