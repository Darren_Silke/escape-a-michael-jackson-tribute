﻿using UnityEngine;

// This script extends the Renderer class, adding an additional method.
// See below.
public static class RendererExtensionsScript
{
	// This method is used by other scripts to determine whether particular
	// objects are within the view of the camera.
	public static bool IsVisibleFrom (this Renderer renderer, Camera camera)
	{
		Plane[] planes = GeometryUtility.CalculateFrustumPlanes (camera);
		return GeometryUtility.TestPlanesAABB (planes, renderer.bounds);
	}
}
