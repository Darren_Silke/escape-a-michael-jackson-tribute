﻿using UnityEngine;

// This script is used to keep track of player data and 
// general game data.
public class GameDataScript : MonoBehaviour
{
	// Internal variables.
	private int score;
	private int enemyCount;
	private GUIStyle font;
	private GameObject michael;
	private GameObject bodyguard;

	// Method called upon initialization.
	void Start ()
	{
		font = new GUIStyle ();
		michael = GameObject.Find ("Michael");
		bodyguard = GameObject.Find ("Bodyguard");

		// Settings for Level 1.
		if (Application.loadedLevelName.Equals ("Level 1")) {
			// Settings for 1 player mode.
			if (PlayerPrefs.GetInt ("Player Mode") == 1) {
				Destroy (bodyguard);
				Destroy (GameObject.Find ("Gun 2"));
			}
			score = 0;
			enemyCount = 8;
			// Settings for Level 2.
		} else if (Application.loadedLevelName.Equals ("Level 2")) {
			michael.GetComponent<HealthScript> ().healthPoints = PlayerPrefs.GetInt ("Michael's Health");
			bodyguard.GetComponent<HealthScript> ().healthPoints = PlayerPrefs.GetInt ("Bodyguard's Health");
			if (bodyguard.GetComponent<HealthScript> ().healthPoints == 0) {
				Destroy (bodyguard);
			}
			score = PlayerPrefs.GetInt ("Score");
			enemyCount = 11;
			// Default settings (not really needed).
		} else {
			score = 0;
			enemyCount = 0;
		}
	}

	// OnGUI method is called for rendering and handling GUI events.
	// Displays the score and player/(s) health on the screen.
	void OnGUI ()
	{
		// Set the font.
		font.fontSize = 30;
		font.normal.textColor = Color.yellow;
		font.fontStyle = FontStyle.Bold;

		// Display the score.
		GUI.Label (new Rect (10, 10, 20, 10), "Score: " + score, font);
		if (michael != null) {
			// Display Michael's health.
			GUI.Label (new Rect (Screen.width - 275, 10, 20, 10), "Michael's Health: " + michael.GetComponent<HealthScript> ().healthPoints, font);
		}
		if (bodyguard != null) {
			// Display the bodyguard's health.
			GUI.Label (new Rect (Screen.width - 323, 50, 20, 10), "Bodyguard's Health: " + bodyguard.GetComponent<HealthScript> ().healthPoints, font);
		}
	}

	// Method to increase the score.
	public void IncreaseScore (int scoreIncrease)
	{
		score += scoreIncrease;
	}

	// Get method to return the score.
	public int Score {
		get {
			return score;
		}
	}

	// Method to decrease the enemy count.
	// Used to determine when to enable the various rewards in the game.
	public void DecreaseEnemyCount ()
	{
		enemyCount--;
		if (enemyCount == 0) {
			GameObject key = GameObject.Find ("Key");
			if (Application.loadedLevelName.Equals ("Level 2")) {
				key.transform.position = new Vector3 (Camera.main.transform.position.x, Camera.main.transform.position.y + 4, 0);
			}
			key.GetComponent<Renderer> ().enabled = true;
			key.GetComponent<PolygonCollider2D> ().enabled = true;
		} else if ((enemyCount == 3) && (Application.loadedLevelName.Equals ("Level 2"))) {
			EnableReward ("Life");
			EnableReward ("Record");
		}
	}

	// Method to load a level when called from another script.
	// Before loading a level, game data such as the score and 
	// the player/(s) health are saved.
	public void LoadLevel (string levelName)
	{
		PlayerPrefs.SetInt ("Score", gameObject.GetComponent<GameDataScript> ().Score);
		PlayerPrefs.SetInt ("Michael's Health", michael.GetComponent<HealthScript> ().healthPoints);
		if (bodyguard != null) {
			PlayerPrefs.SetInt ("Bodyguard's Health", bodyguard.GetComponent<HealthScript> ().healthPoints);
		} else {
			PlayerPrefs.SetInt ("Bodyguard's Health", 0);
		}
		Application.LoadLevel (levelName);
	}
	
	// Method to enable a particular reward.
	public void EnableReward (string rewardName)
	{
		GameObject reward = GameObject.Find (rewardName);
		reward.GetComponent<Renderer> ().enabled = true;
		if (rewardName.Contains ("Life")) {
			reward.GetComponent<BoxCollider2D> ().enabled = true;
		} else {
			reward.GetComponent<CircleCollider2D> ().enabled = true;
		}
		// A reward is destroyed after 5 seconds if the player/(s) have not claimed it.
		Destroy (reward, 5);
	}
}
