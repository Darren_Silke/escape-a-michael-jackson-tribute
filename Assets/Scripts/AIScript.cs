﻿using UnityEngine;

// This script is used for the two AI Enemies. It ensures that the enemies follow a target.
public class AIScript : MonoBehaviour
{
	// The target to follow. 
	// In this game, it's Michael.
	public Transform target;
	// The move speed of the object that this script is attached to.
	public int moveSpeed;
	// The rotation speed of the object that this script is attached to.
	public int rotationSpeed;

	// Method called upon initialization.
	void Start ()
	{
		target = GameObject.Find ("Michael").transform;
	}
	
	// Update is called once per frame.
	void Update ()
	{    
		if (target != null) {
			Vector3 direction = target.position - transform.position;
			// Only needed if objects don't share 'z' value.
			direction.z = 0.0f;
			if (direction != Vector3.zero) {
				transform.rotation = Quaternion.Slerp (transform.rotation, Quaternion.FromToRotation (Vector3.right, direction), rotationSpeed * Time.deltaTime);
			}
			//Move Towards Target.
			transform.position += (target.position - transform.position).normalized * moveSpeed * Time.deltaTime;
		}
		rigidbody2D.velocity = Vector3.zero;
	}
}
