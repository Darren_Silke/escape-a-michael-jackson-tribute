﻿using UnityEngine;

// Michael controller and behaviour.
public class MichaelScript : MonoBehaviour
{
	// The speed of Michael.
	public Vector2 speed = new Vector2 (5, 5);

	// Sprite to display depending on the direction of Michael.
	public Sprite leftSprite;
	public Sprite rightSprite;
	public Sprite upSprite;
	public Sprite downSprite;

	// Store the movement.
	private Vector2 movement;

	// Store the sprite renderer component.
	private SpriteRenderer spriteRenderer;

	// Method called upon initialization.
	void Start ()
	{
		spriteRenderer = GetComponent<SpriteRenderer> ();
		transform.position = new Vector3 (1.5f, -4.6f, 0);
	}

	// Update is called once per frame.
	void Update ()
	{
		// Retrieve axis information.
		float inputX = 0; 
		float inputY = 0;

		// Key codes for Michael's movement.
		if (Input.GetKey (KeyCode.LeftArrow)) {
			inputX = -1;
			spriteRenderer.sprite = leftSprite;
			transform.localScale = new Vector3 (-3, 3, 3);
		} else if (Input.GetKey (KeyCode.RightArrow)) {
			inputX = 1;
			spriteRenderer.sprite = rightSprite;
			transform.localScale = new Vector3 (3, 3, 3);
		} else if (Input.GetKey (KeyCode.UpArrow)) {
			inputY = 1;
			spriteRenderer.sprite = upSprite;
			transform.localScale = new Vector3 (3, 3, 3);
		} else if (Input.GetKey (KeyCode.DownArrow)) {
			inputY = -1;
			spriteRenderer.sprite = downSprite;
			transform.localScale = new Vector3 (3, 3, 3);
		}

		// Movement per direction.
		movement = new Vector2 (speed.x * inputX, speed.y * inputY);

		// Make sure Michael is not outside the screen bounds.
		Vector3 minScreenBounds = Camera.main.ScreenToWorldPoint (new Vector3 (0, 0, 0));
		Vector3 maxScreenBounds = Camera.main.ScreenToWorldPoint (new Vector3 (Screen.width, Screen.height, 0));
		transform.position = new Vector3 (
			Mathf.Clamp (transform.position.x, minScreenBounds.x + 0.4f, maxScreenBounds.x - 0.4f),
			Mathf.Clamp (transform.position.y, minScreenBounds.y + 0.8f, maxScreenBounds.y - 0.8f),
			transform.position.z
		);

		// Shooting.
		bool shoot = Input.GetKey (KeyCode.Space);
		if (shoot) {
			WeaponScript weapon = GetComponent<WeaponScript> ();
			if ((weapon != null) && (weapon.enabled)) {
				// False, because the player is not an enemy.
				weapon.Attack (false, 1);
			}
		} 
	}

	// FixedUpdate is called every physics step.
	void FixedUpdate ()
	{
		// Move the game object.
		rigidbody2D.velocity = movement;
	}

	// Method is called when Michael collides with another object.
	void OnCollisionEnter2D (Collision2D collision)
	{
		bool damagePlayer = false;

		// Collision with enemy or AI enemy.
		EnemyScript enemy = collision.gameObject.GetComponent<EnemyScript> ();
		if (enemy != null) {
			// Damage the enemy.
			HealthScript enemyHealth = enemy.GetComponent<HealthScript> ();
			if (enemyHealth != null) {
				enemyHealth.Damage (enemyHealth.healthPoints);
			}
			damagePlayer = true;
		}

		// Damage the player.
		if (damagePlayer) {
			HealthScript playerHealth = this.GetComponent<HealthScript> ();
			if (playerHealth != null)
				playerHealth.Damage (1);
		}
	}
}
