﻿using UnityEngine;

// Script creates instance of sounds.
public class SoundEffectsHelperScript : MonoBehaviour
{
	// Singleton.
	public static SoundEffectsHelperScript Instance;

	// Sound clips for each type of sound effect in the game.
	public AudioClip michaelHurtSound;
	public AudioClip bodyguardHurtSound;
	public AudioClip explosionSound1;
	public AudioClip explosionSound2;
	public AudioClip playerShootSound;
	public AudioClip enemyShootSound;
	public AudioClip bossEnemyShootSound;
	public AudioClip aiEnemyShootSound;
	public AudioClip healthBoostSound;
	public AudioClip gunCollectionSound;
	public AudioClip michaelRecordCollectionSound;
	public AudioClip bodyguardRecordCollectionSound;
	public AudioClip keyCollectionSound;

	// Method called upon initialization.
	void Awake ()
	{
		// Register the singleton.
		if (Instance != null) {
			Debug.LogError ("Multiple instances of SoundEffectsHelperScript!");
		}
		Instance = this;
	}

	// Method to make the sound when Michael gets hurt.
	public void MakeMichaelHurtSound ()
	{
		MakeSound (michaelHurtSound);
	}

	// Method to make the sound when the bodyguard gets hurt.
	public void MakeBodyguardHurtSound ()
	{
		MakeSound (bodyguardHurtSound);
	}

	// Method to make the sound when an enemy is destroyed.
	public void MakeExplosionSound (string enemyType)
	{
		if ((enemyType.Equals ("Boss Enemy")) || (enemyType.Contains ("AI Enemy"))) {
			MakeSound (explosionSound1);
		} else if (enemyType.Contains ("Enemy")) {
			MakeSound (explosionSound2);
		}
	}

	// Method to make the sound when the player/(s) shoot.
	public void MakePlayerShootSound ()
	{
		MakeSound (playerShootSound);
	}

	// Method to make the sound when the enemies shoot.
	public void MakeEnemyShootSound ()
	{
		MakeSound (enemyShootSound);
	}

	// Method to make the sound when the boss enemy shoots.
	public void MakeBossEnemyShootSound ()
	{
		MakeSound (bossEnemyShootSound);
	}

	// Method to make the sound when the AI enemies shoot.
	public void MakeAIEnemyShootSound ()
	{
		MakeSound (aiEnemyShootSound);
	}

	// Method to make the sound when the player/(s) collect a health boost object.
	public void MakeHealthBoostSound ()
	{
		MakeSound (healthBoostSound);
	}

	// Method to make the sound when the player/(s) collect a gun.
	public void MakeGunCollectionSound ()
	{
		MakeSound (gunCollectionSound);
	}

	// Method to make the sound when Michael collects a record object.
	public void MakeMichaelRecordCollectionSound ()
	{
		MakeSound (michaelRecordCollectionSound);
	}

	// Method to make the sound when the bodyguard collects a record object.
	public void MakeBodyguardRecordCollectionSound ()
	{
		MakeSound (bodyguardRecordCollectionSound);
	}

	// Method to make the sound when the player/(s) collect a key object.
	public void MakeKeyCollectionSound ()
	{
		MakeSound (keyCollectionSound);
	}
	
	// Method to play a given audio clip.
	private void MakeSound (AudioClip originalClip)
	{
		// As it is not a 3D audio clip, position doesn't matter.
		AudioSource.PlayClipAtPoint (originalClip, transform.position);
	}
}
