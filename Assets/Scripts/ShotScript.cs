﻿using UnityEngine;

// Projectile behaviour.
public class ShotScript : MonoBehaviour
{
	// Damage inflicted.
	public int damage = 1;
	
	// Projectile damages the player or the enemies?
	public bool isEnemyShot = false;

	// Method called upon initialization.
	void Start ()
	{
		// Limited time to live to avoid any leak.
		Destroy (gameObject, 2); // 2 seconds.
	}

	// Method is called when the object that this script is attached to has the isTrigger property enabled 
	// and collides with another object.
	void OnTriggerEnter2D (Collider2D otherCollider)
	{
		if ((otherCollider.gameObject.name.Contains ("Wall")) || (otherCollider.gameObject.name.Contains ("Door"))) {
			Destroy (gameObject);
		}
	}
}
