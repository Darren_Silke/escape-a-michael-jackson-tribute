﻿using UnityEngine;

public class RewardScript : MonoBehaviour
{
	// Internal variables to store a random position for where a reward object will spawn.
	private float randomXPosition;
	private float randomYPosition;

	// Internal variable to store game data.
	private GameObject gameData;
	
	// Method called upon initialization.
	void Start ()
	{
		// Ensure that a reward object will spawn in a random position in the scene.
		// The random position must be within the camera view and at an appropriate place.
		Vector3 minScreenBounds = Camera.main.ScreenToWorldPoint (new Vector3 (0, 0, 0));
		Vector3 maxScreenBounds = Camera.main.ScreenToWorldPoint (new Vector3 (Screen.width, Screen.height, 0));

		randomXPosition = Random.Range (minScreenBounds.x + 2.1f, maxScreenBounds.x - 2.2f);
		randomYPosition = Random.Range (minScreenBounds.y + 7, maxScreenBounds.y - 0.5f);
		gameObject.transform.position = new Vector3 (randomXPosition, randomYPosition, 0);

		if ((gameObject.name.Equals ("Gun 1")) || (gameObject.name.Equals ("Gun 2"))) {
			gameObject.GetComponent<Renderer> ().enabled = true;
			gameObject.GetComponent<BoxCollider2D> ().enabled = true;
		}

		gameData = GameObject.Find ("Game Data");
	}

	// Method is called when the object that this script is attached to has the isTrigger property enabled 
	// and collides with another object.
	void OnTriggerEnter2D (Collider2D otherCollider)
	{
		// Checks if the collision occurred between Michael or the bodyguard and the reward object.
		if ((otherCollider.gameObject.name.Equals ("Michael")) || (otherCollider.gameObject.name.Equals ("Bodyguard"))) {
			if (gameObject.name.Contains ("Life")) {
				// Play health boost sound.
				SoundEffectsHelperScript.Instance.MakeHealthBoostSound ();
				// Increase the player's health.
				HealthScript playerHealth = otherCollider.gameObject.GetComponent<HealthScript> ();
				playerHealth.IncreaseHealth (1);
				// Remove the reward object from the scene.
				Destroy (gameObject);
			} else if (gameObject.name.Contains ("Record")) {
				// Play record collection sound.
				if (otherCollider.gameObject.name.Equals ("Michael")) {
					SoundEffectsHelperScript.Instance.MakeMichaelRecordCollectionSound ();
				} else {
					SoundEffectsHelperScript.Instance.MakeBodyguardRecordCollectionSound ();
				}
				gameData.GetComponent<GameDataScript> ().IncreaseScore (5);
				// Remove the reward object from the scene.
				Destroy (gameObject);
			} else if ((gameObject.name.Contains ("Gun")) && (otherCollider.gameObject.GetComponent<WeaponScript> ().enabled == false)) {
				// Play gun collection sound.
				SoundEffectsHelperScript.Instance.MakeGunCollectionSound ();
				// Enable the WeaponScript for the player, which will enable him/her to shoot.
				otherCollider.gameObject.GetComponent<WeaponScript> ().enabled = true;
				// Remove the reward object from the scene.
				Destroy (gameObject);
			} else if (gameObject.name.Equals ("Key")) {
				// Play key collection sound.
				SoundEffectsHelperScript.Instance.MakeKeyCollectionSound ();
				GameObject.Find ("Secret Door").GetComponent<OpenSecretDoorScript> ().OpenSecretDoor ();
				// Remove the reward object from the scene.
				Destroy (gameObject);
			}		
		}
	}
}
