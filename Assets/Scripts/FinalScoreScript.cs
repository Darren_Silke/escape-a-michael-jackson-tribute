﻿using UnityEngine;

// This script is used to display the final score at the end of the game
// if the player/(s) manage to win the game.
public class FinalScoreScript : MonoBehaviour
{
	// Store the font to be used for the score.
	private GUIStyle font;

	// Method called upon initialization.
	void Start ()
	{
		font = new GUIStyle ();
	}

	// OnGUI method is called for rendering and handling GUI events.
	// Displays the final score on the screen.
	void OnGUI ()
	{
		// Set the font.
		font.fontSize = 45;
		font.normal.textColor = Color.yellow;
		font.fontStyle = FontStyle.Bold;
		
		// Display the score.
		GUI.Label (new Rect ((Screen.width / 2) - 160, Screen.height - 60, 20, 10), "Score: " + PlayerPrefs.GetInt ("Score"), font);
	}
}
