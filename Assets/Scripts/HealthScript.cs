﻿using UnityEngine;

// Handle hitpoints and damages.
public class HealthScript : MonoBehaviour
{
	// Total hitpoints.
	public int healthPoints = 1;
	
	// Enemy or player?
	public bool isEnemy = true;

	// Internal variable to keep track of the game data.
	private GameObject gameData;

	// Method called upon initialization.
	void Start ()
	{
		gameData = GameObject.Find ("Game Data");
	}

	// Method is called when the object that this script is attached to collides with another object
	// that has the isTrigger property enabled.
	void OnTriggerEnter2D (Collider2D otherCollider)
	{
		// Is this a shot?
		ShotScript shot = otherCollider.gameObject.GetComponent<ShotScript> ();
		if (shot != null) {
			// Avoid friendly fire.
			if (shot.isEnemyShot != isEnemy) {
				if (gameObject.name.Contains ("Enemy")) {
					gameData.GetComponent<GameDataScript> ().IncreaseScore (1);
				}
				Damage (shot.damage);
				// Destroy the shot.
				// It is important to remember to always target the game object, 
				// otherwise one will just remove the script.
				Destroy (shot.gameObject);
			}
		}
	}

	// Inflicts damage and checks if the object should be destroyed.
	public void Damage (int damageCount)
	{
		if (gameObject.name.Equals ("Michael")) {
			// Michael hurt sound!
			SoundEffectsHelperScript.Instance.MakeMichaelHurtSound ();
		} else if (gameObject.name.Equals ("Bodyguard")) {
			// Bodyguard hurt sound!
			SoundEffectsHelperScript.Instance.MakeBodyguardHurtSound ();
		}
		healthPoints -= damageCount;
		if (healthPoints <= 0) {
			if (gameObject.name.Contains ("Enemy")) {
				gameData.GetComponent<GameDataScript> ().DecreaseEnemyCount ();
				if ((gameObject.name.Equals ("Enemy 2")) && (Application.loadedLevelName.Equals ("Level 1"))) {
					gameData.GetComponent<GameDataScript> ().EnableReward ("Life 1");
				} else if ((gameObject.name.Equals ("Enemy 7")) && (Application.loadedLevelName.Equals ("Level 1"))) {
					gameData.GetComponent<GameDataScript> ().EnableReward ("Life 2");
				} else if ((gameObject.name.Equals ("Enemy 4")) && (Application.loadedLevelName.Equals ("Level 1"))) {
					gameData.GetComponent<GameDataScript> ().EnableReward ("Record 1");
				} else if ((gameObject.name.Equals ("Enemy 5")) && (Application.loadedLevelName.Equals ("Level 1"))) {
					gameData.GetComponent<GameDataScript> ().EnableReward ("Record 2");
				}
			}
			// Explosion sound!
			SoundEffectsHelperScript.Instance.MakeExplosionSound (gameObject.name);
			// Explosion effect!
			SpecialEffectsHelperScript.Instance.Explosion (gameObject.name, transform.position);
			// Dead!
			if (gameObject.name.Equals ("Michael")) {
				gameData.GetComponent<GameDataScript> ().LoadLevel ("Game Over Menu");
			}
			Destroy (gameObject);
		}
	}
	
	// Increases health.
	public void IncreaseHealth (int healthIncrease)
	{
		healthPoints += healthIncrease;
	}
}
