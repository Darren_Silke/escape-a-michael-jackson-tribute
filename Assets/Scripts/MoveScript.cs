﻿using UnityEngine;

// Simply moves the current game object.
public class MoveScript : MonoBehaviour
{
	// Object speed.
	public Vector2 speed = new Vector2 (0, 0);
	
	// Moving direction.
	public Vector2 direction = new Vector2 (0, 0);

	// Internal variable for object movement.
	private Vector2 movement;

	// Update is called once per frame.
	void Update ()
	{
		// Movement.
		movement = new Vector2 (
			speed.x * direction.x,
			speed.y * direction.y);
	}

	// FixedUpdate is called every physics step.
	void FixedUpdate ()
	{
		// Apply movement to the rigidbody.
		rigidbody2D.velocity = movement;
	}
}
