﻿using UnityEngine;

// This script is used to open the door at the end of a level.
public class OpenSecretDoorScript : MonoBehaviour
{
	// The sprite to display once the door is open.
	public Sprite openDoorSprite;

	// Store the sprite renderer component.
	private SpriteRenderer spriteRenderer;

	// Internal variables.
	private bool levelComplete;
	private GameObject gameData;

	// Method called upon initialization.
	void Start ()
	{
		spriteRenderer = GetComponent<SpriteRenderer> ();
		gameData = GameObject.Find ("Game Data");
	}

	// Method is called when the object that this script is attached to has the isTrigger property enabled 
	// and collides with another object.
	void OnTriggerEnter2D (Collider2D otherCollider)
	{
		if ((levelComplete) && (otherCollider.name.Equals ("Michael"))) {
			if (Application.loadedLevelName.Equals ("Level 1")) {
				gameData.GetComponent<GameDataScript> ().LoadLevel ("Level 2");
			} else {
				gameData.GetComponent<GameDataScript> ().LoadLevel ("Game Completed Menu");
			}
		}
	}

	// Method to open the door.
	public void OpenSecretDoor ()
	{
		spriteRenderer.sprite = openDoorSprite;
		gameObject.GetComponent<EdgeCollider2D> ().isTrigger = true;
		levelComplete = true;
	}
}
