﻿using UnityEngine;

// This script is used to control the movement of the camera.
public class CameraScript : MonoBehaviour
{
	// The delay in seconds for the camera to move.
	public float dampTime = 0.5f;
	// The target for the camera to follow.
	// In this game, the target is Michael.
	public Transform target;

	// The initial velocity of the camera.
	private Vector3 velocity = Vector3.zero;

	// FixedUpdate is called every physics step.
	void FixedUpdate ()
	{
		// Acquire target, and move camera to appropriate position.
		if (target) {
			Vector3 point = camera.WorldToViewportPoint (target.position);
			Vector3 delta = target.position - camera.ViewportToWorldPoint (new Vector3 (0.5f, 0.5f, point.z));
			Vector3 destination = transform.position + delta;
			transform.position = Vector3.SmoothDamp (transform.position, destination, ref velocity, dampTime); 
		}

		// Stop camera movement at particular points along X-axis.
		if (transform.position.x <= 0) {
			transform.position = new Vector3 (0, transform.position.y, transform.position.z);
		} else if (transform.position.x >= 3) {
			transform.position = new Vector3 (3, transform.position.y, transform.position.z);
		}

		// Camera movement boundary settings for Level 1 along the Y-axis.
		if (Application.loadedLevelName.Equals ("Level 1")) {
			// Stop camera movement at particular points along Y-axis.
			if (transform.position.y <= -0.4f) {
				transform.position = new Vector3 (transform.position.x, -0.4f, transform.position.z);
			} else if (transform.position.y >= 3.2f) {
				transform.position = new Vector3 (transform.position.x, 3.2f, transform.position.z);
			}
		}
		// Camera movement boundary settings for Level 2 along the Y-axis.
		else if (Application.loadedLevelName.Equals ("Level 2")) {
			// Stop camera movement at particular points along Y-axis.
			if (transform.position.y <= -0.4f) {
				transform.position = new Vector3 (transform.position.x, -0.4f, transform.position.z);
			} else if (transform.position.y >= 15.6f) {
				transform.position = new Vector3 (transform.position.x, 15.6f, transform.position.z);
			}
		}
	}
}
