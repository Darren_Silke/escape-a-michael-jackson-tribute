﻿using UnityEngine;

// This script is used to handle the behaviour of the buttons in the various menus.
public class ButtonScript : MonoBehaviour
{
	// Method is called when a button is clicked. 
	public void ClickButton (string buttonID)
	{
		if ((buttonID.Equals ("Play")) || (buttonID.Equals ("Replay")) || (buttonID.Equals ("Try Again"))) {
			Application.LoadLevel ("Play Menu");
		} else if (buttonID.Equals ("Help")) {
			Application.LoadLevel ("Help Menu");
		} else if (buttonID.Equals ("1 Player")) {
			// Set the player mode to 1 player.
			PlayerPrefs.SetInt ("Player Mode", 1);
			Application.LoadLevel ("Level 1");
		} else if (buttonID.Equals ("2 Player")) {
			// Set the player mode to 2 player.
			PlayerPrefs.SetInt ("Player Mode", 2);
			Application.LoadLevel ("Level 1");
		} else if (buttonID.Equals ("Return To Main Menu")) {
			Application.LoadLevel ("Main Menu");
		}
		// If the "Exit" button is clicked...
		else {
			Application.Quit ();
		}
	}
}
