﻿using UnityEngine;

// Script creates instance of particles.
public class SpecialEffectsHelperScript : MonoBehaviour
{
	// Singleton.
	public static SpecialEffectsHelperScript Instance;
	public ParticleSystem enemyExplosionEffect;
	public ParticleSystem bossEnemyExplosionEffect;

	// Method called upon initialization.
	void Awake ()
	{
		// Register the singleton.
		if (Instance != null) {
			Debug.LogError ("Multiple instances of SpecialEffectsHelperScript!");
		}
		Instance = this;
	}
	
	// Create an explosion at the given location.
	public void Explosion (string enemyType, Vector3 position)
	{
		if (enemyType.Equals ("Boss Enemy")) {
			Instantiate (bossEnemyExplosionEffect, position);
		} else if (enemyType.Contains ("Enemy")) {
			Instantiate (enemyExplosionEffect, position);
		}
	}
	
	// Instantiate a Particle system from a prefab.
	private ParticleSystem Instantiate (ParticleSystem prefab, Vector3 position)
	{
		ParticleSystem newParticleSystem = Instantiate (prefab, position, Quaternion.identity) as ParticleSystem;
		// Make sure it will be destroyed.
		Destroy (newParticleSystem.gameObject, newParticleSystem.startLifetime);
		return newParticleSystem;
	}
}
