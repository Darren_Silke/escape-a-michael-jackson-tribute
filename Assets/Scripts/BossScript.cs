﻿using UnityEngine;

// Enemy generic behaviour.
public class BossScript : MonoBehaviour
{
	private bool hasSpawn;

	// Component references.
	private MoveScript moveScript;
	private WeaponScript[] weapons;

	// Boss pattern.
	public float minAttackCooldown = 0.5f;
	public float maxAttackCooldown = 2f;

	// Boss behaviour.
	private float aiCooldown;
	private bool isAttacking;
	private Vector2 positionTarget;

	// Method called upon initialization.
	// Method retrieves references between scripts.
	void Awake ()
	{
		// Retrieve the weapon only once.
		weapons = GetComponentsInChildren<WeaponScript> ();

		// Retrieve scripts to disable when not spawned.
		moveScript = GetComponent<MoveScript> ();
	}

	// Method called upon initialization.
	void Start ()
	{
		hasSpawn = false;

		// Disable everything.
		// -- Collider.
		collider2D.enabled = false;
		// -- Moving.
		moveScript.enabled = false;
		// -- Shooting.
		foreach (WeaponScript weapon in weapons) {
			weapon.enabled = false;
		}

		// Default behaviour.
		isAttacking = false;
		aiCooldown = maxAttackCooldown;
	}

	// Update is called once per frame.
	void Update ()
	{
		// Check if the enemy has spawned.
		if (hasSpawn == false) {
			if (renderer.IsVisibleFrom (Camera.main)) {
				Spawn ();
			}
		} else {
			// AI:
			// Move or attack. Repeat.
			aiCooldown -= Time.deltaTime;

			if (aiCooldown <= 0f) {
				isAttacking = !isAttacking;
				aiCooldown = Random.Range (minAttackCooldown, maxAttackCooldown);
				positionTarget = Vector2.zero;
			}

			// Attack:
			if (isAttacking) {
				// Stop any movement.
				moveScript.direction = Vector2.zero;

				foreach (WeaponScript weapon in weapons) {
					if ((weapon != null) && (weapon.enabled) && (weapon.CanAttack)) {
						weapon.Attack (true, 0);
					}
				}
			}

			// Move:
			else {
				// Define a target.
				if (positionTarget == Vector2.zero) {
					// Get a point on the screen, convert to world.
					Vector2 randomPoint = new Vector2 (Random.Range (0f, 1f), Random.Range (0f, 1f));
					positionTarget = Camera.main.ViewportToWorldPoint (randomPoint);
				}

				// Are we at the target? If so, find a new one.
				if (collider2D.OverlapPoint (positionTarget)) {
					// Reset, will be set at the next frame.
					positionTarget = Vector2.zero;
				}

				// Go to the point.
				Vector3 direction = ((Vector3)positionTarget - this.transform.position);

				// Remember to use the move script.
				moveScript.direction = Vector3.Normalize (direction);
			}
		}
	}

	// Spawn method to make the boss spawn.
	private void Spawn ()
	{
		hasSpawn = true;

		// Enable everything.
		// -- Collider.
		collider2D.enabled = true;
		// -- Moving.
		moveScript.enabled = true;
		// -- Shooting.
		foreach (WeaponScript weapon in weapons) {
			weapon.enabled = true;
		}
	}

	// Method is called when another object that has the isTrigger property set to true collides with the boss. 
	void OnTriggerEnter2D (Collider2D otherCollider2D)
	{
		// Taking damage?
		ShotScript shot = otherCollider2D.gameObject.GetComponent<ShotScript> ();
		if (shot != null) {
			if (shot.isEnemyShot == false) {
				// Stop attacks and start moving away.
				aiCooldown = Random.Range (minAttackCooldown, maxAttackCooldown);
				isAttacking = false;
			}
		}
	}

	// Method is used to display debug information in a scene with Gizmos.
	void OnDrawGizmos ()
	{
		if ((hasSpawn) && (isAttacking == false)) {
			// The boss's target position is displayed using a sphere.
			Gizmos.DrawSphere (positionTarget, 0.25f);
		}
	}
}
