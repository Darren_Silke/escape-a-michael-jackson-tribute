Name:		Darren Silke
Student Number: SLKDAR001
Date:		5 April 2015

Game Name: Escape - A Michael Jackson Tribute

Description:

This game has both single and two player modes. The objective of the game in both modes of play, is to kill all the enemies while attempting to achieve a high score and essentially escape the building in which the players are trapped. Player 1 is Michael Jackson and player 2 is Michael Jackson's bodyguard.

The game is divided into two levels. Upon completion of each level, a key will appear which needs to be picked up by a player. Once the key is picked up, a door will be unlocked and opened. Once Michael Jackson steps through the door, the player/(s) will be transported to the next level. If the player/(s) have completed the last level, the player/(s) will be taken to a menu indicating that the game has been won and the player/(s) score will be displayed.

Throughout the game, the player/(s) will be presented with objects that will appear at certain points during gameplay as rewards for killing enemies. There are two types of objects, a health boost object and a record object. Upon acquiring the health boost object, a player's health will be increased. Upon acquiring the record object, the score will be increased.

1 Player Mode:
==============

Michael Jackson's (player 1) movement is controlled by the arrow keys on the keyboard. The up arrow key is for up, the down arrow key is for down, the left arrow key is for left and the right arrow key is for right.
In order to shoot, a gun must first be acquired. Once acquired, the space key is used for shooting.

2 Player Mode:
==============

Michael Jackson (player 1) is controlled the same way as described above for 1 player mode.

Michael Jackson's bodyguard (player 2) movement is controlled by the following keys:
W = Up movement
S = Down movement
A = Left movement
D = Right movement
In order to shoot, a gun must first be acquired. Once acquired, the left ctrl key is used for shooting.

Instructions:

1. Run the game using the packaged .exe file in an appropriate widescreen resolution.
2. Follow the on screen instructions in terms of clicking on the appropriate buttons. 
3. To exit the game, click the "Exit" button in the main menu. To navigate to the main menu, either win or lose the game, or click "Return To Main Menu" if in another menu other than the main menu.